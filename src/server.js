// Criação do arquivo de Servidor
//INÍCIO DO PROJETO
require("express-async-errors");
const migrationsRun = require("./database/sqlite/migrations");
const AppError = require("./utils/AppError");

const express = require("express"); // Importação do express lá do 'node_modules'

const routes = require("./routes"); // Importação do arquivo 'routes'
migrationsRun();

const app = express(); // Inicializando o express...
app.use(express.json());


app.use(routes);


app.use((error, request, response, next) => {
    if(error instanceof AppError){
        return response.status(error.statusCode).json({
            status: "error",
            message: error.message
        });
    };
});

const PORT = 3333; // Escolhendo a porta que iremos verificar
app.listen(PORT, () => console.log(`Server is running on Port ${PORT}`)); // Escutando a resposta para escrever a mensagem...



