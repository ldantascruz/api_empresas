const AppError = require("../utils/AppError");

const sqliteConnection = require("../database/sqlite");
const { default: knex } = require("knex");

class CompaniesController {
    async create(request, response) {
        const { 
            id, 
            razaoSocial, 
            nomeFantasia, 
            cnpj, 
            e_logradouro,
            e_numero,
            e_complemento,
            e_bairro,
            e_cidade,
            e_estado,
            telefone, 

        } = request.body;

        const database = await sqliteConnection();
        const checkCompanieExists = await database.get("SELECT * FROM empresas WHERE cnpj = (?)", [cnpj]);

        if (checkCompanieExists){
            throw new AppError("Este CNPJ já está cadastrado!");
        }

        await database.run(
            "INSERT INTO empresas (razaoSocial, nomeFantasia, cnpj, e_logradouro, e_numero, e_complemento, e_bairro, e_cidade, e_estado, telefone) VALUES (?,?,?,?,?,?,?,?,?,?)",
            [razaoSocial, nomeFantasia, cnpj, e_logradouro, e_numero, e_complemento, e_bairro, e_cidade, e_estado, telefone],
        );

        return response.status(201).json();
        
    };

    async update(request, response) {
        const {
            razaoSocial, 
            nomeFantasia, 
            cnpj, 
            e_logradouro, 
            e_numero, 
            e_complemento, 
            e_bairro, 
            e_cidade, 
            e_estado, 
            telefone
        } = request.body;
        const {id} = request.params;

        const database = await sqliteConnection();
        const companie = await database.get("SELECT * FROM empresas WHERE id = (?)", [id]);

        if (!companie) {
            throw new AppError("Empresa não encontrada");
        };

        const companieWithUpdateCnpj = await database.get("SELECT * FROM empresas WHERE cnpj = (?)", [cnpj]);

        if(companieWithUpdateCnpj && companieWithUpdateCnpj.id !== companie.id){
            throw new AppError("Este CNPJ já está cadastrado.");
        };


        companie.razaoSocial = razaoSocial ?? companie.razaoSocial,
        companie.nomeFantasia = nomeFantasia ?? companie.nomeFantasia,
        companie.cnpj = cnpj ?? companie.cnpj,
        companie.e_logradouro = e_logradouro ?? companie.e_logradouro,
        companie.e_numero = e_numero ?? companie.e_numero,
        companie.e_complemento = e_complemento ?? companie.e_complemento,
        companie.e_bairro = e_bairro ?? companie.e_bairro,
        companie.e_cidade = e_cidade ?? companie.e_cidade,
        companie.e_estado = e_estado ?? companie.e_estado,
        companie.telefone = telefone ?? companie.telefone

        await database.run(`
            UPDATE empresas SET
            razaoSocial = ?, 
            nomeFantasia = ?, 
            cnpj = ?, 
            e_logradouro = ?, 
            e_numero = ?, 
            e_complemento = ?, 
            e_bairro = ?, 
            e_cidade = ?, 
            e_estado = ?, 
            telefone = ?
            WHERE id = ?`,
            [
                companie.razaoSocial, 
                companie.nomeFantasia, 
                companie.cnpj, 
                companie.e_logradouro,
                companie.e_numero,
                companie.e_complemento,
                companie.e_bairro,
                companie.e_cidade,
                companie.e_estado,
                companie.telefone,
                id
            ],
        );
        return response.status(200).json();
    };

    async delete(request, response) {
        const { id } = request.params;

        await knex('empresas').where({ id }).delete();

        return response.json();
    };

    async index(request, response) {
        const { id } = request.query;

        const empresa = await knex('empresas')
        .where({ id })
        .orderBy("razaoSocial");
        
        return response.status(200).json(empresa)
        
    };
};

module.exports = CompaniesController;