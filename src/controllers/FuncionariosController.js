const knex = require('../database/knex');

class FuncionariosController {
    async create(request, response){
        const { nome, cpf, idade, create_at, update_at } = request.body;
        const { empresa_id } = request.params;

        const funcionario_id = await knex("funcionarios").insert({
            nome,
            cpf,
            idade,
            empresa_id           
        });

        await knex("funcionarios").insert(funcionario_id);
            
        response.status(201).json();
        
        
    };
};

module.exports = FuncionariosController;