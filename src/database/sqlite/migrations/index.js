const sqliteConnection = require("../../sqlite");
const createCompanies = require("./createCompanies");

const creareCompanies = require("./createCompanies");

async function migrationsRun(){
    const schemas = [
        createCompanies
    ].join('');
    sqliteConnection()
    .then(db => db.exec(schemas))
    .catch(error => console.log(error));
}

module.exports = migrationsRun;