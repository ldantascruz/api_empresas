const createCompanies = `
    CREATE TABLE IF NOT EXISTS empresas (
	id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
  	razaoSocial VARCHAR NOT NULL,
  	nomeFantasia VARCHAR NOT NULL,
  	cnpj VARCHAR NOT NULL,
  	e_logradouro VARCHAR NOT NULL,
  	e_numero VARCHAR NOT NULL,
  	e_complemento VARCHAR NULL,
  	e_bairro VARCHAR NOT NULL,
  	e_cidade VARCHAR NOT NULL,
  	e_estado VARCHAR NOT NULL,
  	telefone VARCHAR NOT NULL
)
`;

module.exports = createCompanies;