exports.up = knex => knex.schema.createTable("funcionarios", table => {
    table.increments("id");
    table.text("nome");
    table.text("cpf");
    table.int("idade");
    table.integer("empresa_id").references("id").inTable("empresas").onDelete("CASCADE");

    table.timestamp("created_at").default(knex.fn.now());
    table.timestamp("updated_at").default(knex.fn.now());
});

exports.down = knex => knex.schema.dropTable("funcionarios");