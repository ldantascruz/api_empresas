const { Router } = require("express");

const FuncionariosController = require("../controllers/FuncionariosController")

const funcRoutes = Router();

const funcionariosController = new FuncionariosController();

funcRoutes.post("/:empresa_id", funcionariosController.create);

module.exports = funcRoutes;