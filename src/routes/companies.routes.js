const { Router } = require("express");

const CompaniesController = require("../controllers/CompaniesController")

const compRoutes = Router();

const companiesController = new CompaniesController();

compRoutes.get("/", companiesController.index);
compRoutes.post("/", companiesController.create);
compRoutes.put("/:id", companiesController.update);
compRoutes.delete("/:id", companiesController.delete);


module.exports = compRoutes;