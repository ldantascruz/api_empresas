const { Router } = require("express");

const compRouter = require("./companies.routes");
const funcRouter = require("./funcionarios.routes");

const routes = Router();

routes.use("/companies", compRouter);
routes.use("/funcionarios", funcRouter);

module.exports = routes; 