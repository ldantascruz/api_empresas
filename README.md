# API Empresas

Um projeto BackEnd para ler, criar, atualizar, e deletar empresas no banco de dados (CRUD);

## Getting Started

Esse projeto foi desenvolvido utilizando o NodeJS

É necessário já ter o node instalado na máquina.

Passo a passo:
1 - Logo após abrir o repositório, abra o terminal e execute o seguinte comando: 'npm run dev'. O Node irá instalar as dependências do projeto
2 - Logo após iniciar o projeto instale algum 'testador de rotas' (eu utilizo o Insomnia) no seu dispositivo para fazer as requisições para o banco de dados.
3 - Teste a aplicação com o testador de rotas;

